Proof of concept. In this project a file upload page is created in react. There you can submit a file and then upload it. The file is moved into the `/client/public/uploads folder.

This project has a **nodejs** server as the backend that serves up the upload page and move the uploaded file.

## Upload files

###

Ensure that nodejs is installed on your system. The download page is [here](https://nodejs.org/en/download/).

Install neccessary packages with `npm install` in the root folder and in the client folder. Those folders have the `package.json` file that npm (node package manager) uses to bring the project dependencies in.

Start dev server and serve react page `npm run dev`.
