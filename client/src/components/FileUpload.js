import React, { Fragment, useState } from 'react'
import Message from './Message'
import Progress from './Progress'
import axios from 'axios'

const FileUpload = () => {
  const [file, setFile] = useState('')
  const [fileName, setFileName] = useState('Choose File')
  const [uploadedFile, setUploadedFile] = useState({})
  const [message, setMessage] = useState('')
  const [uploadPercentage, setUploadPercentage] = useState(0)

  const onChange = (e) => {
    setFile(e.target.files[0])
    setFileName(e.target.files[0].name)
  }

  const onSubmit = async (e) => {
    e.preventDefault()
    const formData = new FormData()
    formData.append('file', file)

    try {
      const res = await axios.post('/upload', formData, {
        headers: { 'Content-Type': 'multipart/form-data' },
        onUploadProgress: (progressEvent) => {
          // prevent setting `uploadpercentage` to
          // 100% on error.
          try {
            setUploadPercentage(
              parseInt(
                Math.round((progressEvent.loaded * 100) / progressEvent.total)
              )
            )
            // Clear Message
            setTimeout(() => {
              setUploadPercentage(0)
            }, 10000)
          } catch (error) {
            setUploadPercentage(0)
          }
        }
      })
      const { fileName, filePath } = res.data

      setUploadedFile({ fileName, filePath })

      setMessage('File Uploaded')
    } catch (error) {
      if (error.status === 500) {
        setMessage('There was a problem with the server')
      } else {
        setMessage(error.response.data.msg)
      }
    }
  }
  console.log(uploadPercentage)
  return (
    <Fragment>
      {message ? <Message msg={message} /> : null}
      <form onSubmit={onSubmit}>
        <div className='custom-file mb-4'>
          <input
            type='file'
            className='custom-file-input'
            id='customFile'
            onChange={onChange}
          />
          <label className='custom-file-label' htmlFor='customFile'>
            {fileName}
          </label>
        </div>
        <Progress percentage={uploadPercentage} />
        <input
          className='btn btn-primary btn-block mt-4'
          type='submit'
          value='Upload'
        />
      </form>
      {Object.keys(uploadedFile).length !== 0 && (
        <div className='row mt-5'>
          <div className='col-md-6 m-auto'>
            <h3 className='text-center'>{uploadedFile.fileName}</h3>
            <img
              style={{ width: '100%' }}
              src={uploadedFile.filePath}
              alt='file'
            />
          </div>
        </div>
      )}
    </Fragment>
  )
}

export default FileUpload
